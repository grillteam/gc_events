<?php
namespace Grillcode;

/**
* Plugin Name: CSDA Events CPT and CMB2
* Plugin URI: http://www.grillcode.com/csda-events-cpt-cmb2
* Version: 0.1.0
* Author: Javier Otero
* Author URI: http://grillcode.com
* Text Domain: grillcode
* Description: Events management plugin
* License: GPL2
* Bitbucket Plugin URI: https://bitbucket.org/grillcode/gc_events_cpt
* Bitbucket Branch: master
*/
defined( 'ABSPATH' ) or die();

require_once plugin_dir_path( __FILE__ ) . 'classes/core.php';

//testing
new Core('0.1.0');


//where should i put this?
require_once 'inc/gcfields.php';

if ( file_exists( dirname( __FILE__ ) . '/inc/CMB2/init.php' ) ) {
	require_once 'inc/CMB2/init.php';
} else {
	add_action( 'admin_notices', 'missing_cmb2' );
}

/**
 * Load CMB2 metaboxes
 */
require_once 'inc/local-user-avatar.php';
require_once 'inc/more-about-user.php';
require_once 'inc/user-social-media-urls.php';
require_once 'inc/event-meta.php';
require_once 'inc/location-meta.php';
require_once 'inc/cmb2-frontend.php';

/**
 * Add an error notice to the dashboard if CMB2 is missing from the plugin
 *
 * @return void
 */
function missing_cmb2() { ?>
	<div class="error">
		<p><?php _e( 'CMB2 is missing!', 'grillcode' ); ?></p>
	</div>
<?php }

new Event();