<?php
namespace Grillcode;

require_once plugin_dir_path( __FILE__ ) . 'admin.php';
require_once plugin_dir_path( __FILE__ ) . 'event.php';
require_once plugin_dir_path( __FILE__ ) . 'user.php';
require_once plugin_dir_path( __FILE__ ) . 'helper.php';
require_once plugin_dir_path( __FILE__ ) . 'invoice.php';
require_once plugin_dir_path( __FILE__ ) . 'payment.php';
require_once plugin_dir_path( __FILE__ ) . 'report.php';
require_once plugin_dir_path( __FILE__ ) . 'mail.php';
require_once plugin_dir_path( __FILE__ ) . 'log.php';


class Core {

	private static $_instance;

	public static function getInstance() {
		if ( !self::$_instance ) {
			self::$_instance = new Core();
		}

		return self::$_instance;
	}

    function __construct() {
 
 		//add_action( 'init', array(&$this, 'add_custom_post_types') );

	}
 

    public function enqueue_styles() {
     
    }
 
    public function add_meta_box() {
     
    }
 
}