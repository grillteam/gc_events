<?php
namespace Grillcode;

/**
 * Event management
 */


class Event {

    /**
     * The plugin's version
     *
     * @var string
     */
    const VERSION = '1.0.0';

    private static $_instance;

    public static function getInstance() {
        if ( !self::$_instance ) {
            self::$_instance = new Event();
        }

        return self::$_instance;
    }

    public function version() {
        return self::VERSION;
    }

    function __construct() {

        add_filter( 'init', array( $this, 'register_cpts') );

        //add_action( 'wp_enqueue_scripts', call_user_func( array( $this, 'enqueue_scripts' )) );
    }
    
    public function register_cpts() {
        
        self::register_event_post_type();
        self::register_location_post_type();

    }
    
    
    public function register_event_post_type() {

        register_post_type( 'gc-event', array(
            'label'               => __( 'Event', 'grillcode' ),
            'description'         => __( 'Event manager post type', 'grillcode' ),
            'capability_type'     => 'post',
            'public'			  => true,
            'has_archive' 		  => true,
            'rewrite'             => array('slug' => 'event'),
            'query_var'           => true,
            'supports'            => array( 'title', 'editor', 'thumbnail' ),
            'labels' => array(
                'name' => __( 'Events', 'grillcode' ),
                'singular_name'      => __( 'Event', 'grillcode' ),
                'menu_name'          => __( 'Event', 'grillcode' ),
                'add_new'            => __( 'Add Event', 'grillcode' ),
                'add_new_item'       => __( 'Add New Event', 'grillcode' ),
                'edit'               => __( 'Edit', 'grillcode' ),
                'edit_item'          => __( 'Edit Event', 'grillcode' ),
                'new_item'           => __( 'New Event', 'grillcode' ),
                'view'               => __( 'View Event', 'grillcode' ),
                'view_item'          => __( 'View Event', 'grillcode' ),
                'search_items'       => __( 'Search Event', 'grillcode' ),
                'not_found'          => __( 'No Event Found', 'grillcode' ),
                'not_found_in_trash' => __( 'No Event Found in Trash', 'grillcode' ),
                'parent'             => __( 'Parent Event', 'grillcode' ),
            ),
        ) );

        register_taxonomy('Event_category', 'gc-event', array(
            'hierarchical' => true,
            'labels' => array(
                'name'              => _x( 'Event Categories', 'taxonomy general name' ),
                'singular_name'     => _x( 'Location', 'taxonomy singular name' ),
                'search_items'      => __( 'Search Event Categories', 'grillcode' ),
                'all_items'         => __( 'All Event Categories', 'grillcode' ),
                'parent_item'       => __( 'Parent Event Category', 'grillcode' ),
                'parent_item_colon' => __( 'Parent Event Category:', 'grillcode' ),
                'edit_item'         => __( 'Edit Event Category', 'grillcode' ),
                'update_item'       => __( 'Update Event Category', 'grillcode' ),
                'add_new_item'      => __( 'Add New Event Category', 'grillcode' ),
                'new_item_name'     => __( 'New Event Category Name', 'grillcode' ),
                'menu_name'         => __( 'Categories', 'grillcode' ),
            ),
            'rewrite' => array(
                'slug'         => 'Event-category',
                'with_front'   => false,
                'hierarchical' => true
            ),
        ));

    }

    public function register_location_post_type() {

        register_post_type( 'gc-location', array(
            'label'               => __( 'Location', 'grillcode' ),
            'description'         => __( 'Location post type', 'grillcode' ),
            'capability_type'     => 'post',
            'public'			  => true,
            'rewrite'             => array('slug' => 'Location'),
            'query_var'           => true,
            'supports'            => array( 'title', 'thumbnail' ),
            'labels' => array(
                'name' => __( 'Locations', 'grillcode' ),
                'singular_name'      => __( 'Location', 'grillcode' ),
                'menu_name'          => __( 'Location', 'grillcode' ),
                'add_new'            => __( 'Add Location', 'grillcode' ),
                'add_new_item'       => __( 'Add New Location', 'grillcode' ),
                'edit'               => __( 'Edit', 'grillcode' ),
                'edit_item'          => __( 'Edit Location', 'grillcode' ),
                'new_item'           => __( 'New Location', 'grillcode' ),
                'view'               => __( 'View Location', 'grillcode' ),
                'view_item'          => __( 'View Location', 'grillcode' ),
                'search_items'       => __( 'Search Location', 'grillcode' ),
                'not_found'          => __( 'No Location Found', 'grillcode' ),
                'not_found_in_trash' => __( 'No Location Found in Trash', 'grillcode' ),
                'parent'             => __( 'Parent Location', 'grillcode' ),
            ),
        ) );

    }
    
    /**
     * Enqueue each of the scripts
     *
     * @since 1.0.0
     *
     * @return null
     */
    public function enqueue_scripts() {

        wp_enqueue_script(
            'gc_events_js',
            plugin_dir_url(dirname(__FILE__)) . '/assets/events.js',
            array( 'jquery' ),
            self::VERSION,
            true
        );
        $params = array(
            'ajaxurl' => admin_url( '/admin-ajax.php' ),
        );
        wp_localize_script( 'gc_events_js', 'gc_events_core', $params );

        wp_enqueue_style( 'gc_events_css', plugin_dir_url(dirname(__FILE__)) . '/assets/events.css', array(), self::VERSION );

    }

}