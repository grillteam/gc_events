
# Events plugin

== Description ==

Migration of an event management application made with ASP.NET to a Wordpress Plugin for managing events, invoices, reports, etc.

== Features (basic list) ==

Registration and handling on front-end

All normal event stuff plus:

## Events
  * Bulk register of attendees
  * Wait list if the event is full booked
  * Manual override of wait list
  * Notifications to attendees by mail
  * Cancellations
  * History of events/attendees
  * Mass email to registrants
  * Printing Certificates and sign in sheets

## Users
  * Front-end Registration
  * Associations can choose and add attendees to a new event from previous events
  * Handling users

## Invoices
  * Generate invoices
  * Sending invoices

## Reports
  * Activities
  * Income
  * Attendees
  * Unpaid
  * Export to PDF and CSV


## Payments
  * Payments with Paypal and credit card
  * Refunds
  * Log


And more things to come...
