<?php
/**
 * Add additional meta to Events cpt
 */
if( ! function_exists( 'event_meta' ) ) {
	function event_meta( ) {
		$prefix = '_gc_event_meta_';

		$cmb = new_cmb2_box( array(
			'id'           => 'event-meta',
			'title'        => __( 'Event data', 'grillcode' ),
			'object_types' => array( 'gc-event' ),
			'show_names'   => true
		) );

		$cmb->add_field( array(
			'name' => __( 'Event Short Name','grillcode' ),
			'id'   => $prefix . 'short_name',
			'type' => 'text_medium'
		) );

		$cmb->add_field( array(
			'name' => __( 'Show Outlook reminder','grillcode' ),
			'id'   => $prefix . 'show_outlook_reminder',
			'type' => 'checkbox'
		) );

		$cmb->add_field( array(
			'name' => __( 'Enable Waitlist','grillcode' ),
			'id'   => $prefix . 'waitlist_enabled',
			'type' => 'checkbox'
		) );

		$cmb->add_field( array(
			'name' => __( 'All day event','grillcode' ),
			'id'   => $prefix . 'all_day',
			'type' => 'checkbox'
		) );

		$cmb->add_field( array(
			'name' => __( 'Event Start','grillcode' ),
			'id'   => $prefix . 'start_date',
			'type' => 'text_date_timestamp'
		) );

		$cmb->add_field( array(
			'name' => __( 'Event End','grillcode' ),
			'id'   => $prefix . 'end_date',
			'type' => 'text_date_timestamp'
		) );

		$cmb->add_field( array(
			'name' => __( 'Registration Deadline','grillcode' ),
			'id'   => $prefix . 'registration_deadline',
			'type' => 'text_date_timestamp',
			// 'timezone_meta_key' => 'wiki_test_timezone',
			// 'date_format' => 'l jS \of F Y',
		) );

		$cmb->add_field( array(
			'name' => __( 'Cancellation Deadline','grillcode' ),
			'id'   => $prefix . 'cancellation_deadline',
			'type' => 'text_date_timestamp'
		) );

		$cmb->add_field( array(
			'name' => __( 'Event Logo','grillcode' ),
			'id'   => $prefix . 'logo',
			'type'    => 'file',
			// Optional:
			'options' => array(
				'url' => false, // Hide the text input for the url
				'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
			)

		) );

		$cmb->add_field( array(
			'name' => __( 'Event Overview','grillcode' ),
			'desc' => 'Event Overview – Here you will add text that describes the event in broad terms (I.e. the general location, purpose and perhaps the cost). This text will be displayed to all users when they access they site – they do NOT have to be logged in to see this text',
			'id' => $prefix . 'overview',
			'type' => 'textarea'
		) );

		/*
		$meta_boxes['cmb2_example_post_media'] = array(
			'id'               => 'cmb2_example_post_media',
			'title'            => __( 'Additional Media', 'cmb2-example-plugin' ),
			'object_types'     => array( 'gc-event' ), // These fields should be placed on the USER object.
			'show_names'       => true,
			'fields'           => array(
				array(
					'name'     => __( 'Additional Files', 'cmb2-example-plugin' ),
					'desc'     => __( 'field description (optional)', 'cmb2-example-plugin' ),
					'id'       => $prefix . 'files',
					'type'     => 'file_list',
				),
				array(
					'name'     => __( 'Video', 'cmb2-example-plugin' ),
					'desc'     => sprintf( __( 'Add a link to any video from a <a href="%1$s" target="_blank">supported provider</a> and a preview will automatically appear below.', 'cmb2-example-plugin' ), 'http://codex.wordpress.org/Embeds' ),
					'id'       => $prefix . 'youtube',
					'type'     => 'oembed',
				),
				array(
					'name'     => __( 'Birthday', 'cmb2-example-plugin' ),
					'desc'     => __( 'field description (optional)', 'cmb2-example-plugin' ),
					'id'       => $prefix . 'birthday',
					'type'     => 'text_date',
					// 'date_format' => __( 'd-m-Y', 'cmb2' ) // Pass a custom date format to use european date format
				),
				array(
					'name' => 'Test Date Picker (UNIX timestamp)',
					'id'   => $prefix . 'textdate_timestamp',
					'type' => 'text_date_timestamp',
					// 'timezone_meta_key' => 'wiki_test_timezone',
					// 'date_format' => 'l jS \of F Y',
				),
				array(
					'name' => 'Test Date/Time Picker Combo (UNIX timestamp)',
					'id'   => $prefix . 'datetime_timestamp',
					'type' => 'text_datetime_timestamp',
				),
				array(
					'name' => 'Test Date/Time Picker/Time zone Combo (serialized DateTime object)',
					'id'   => $prefix . 'datetime_timestamp_timezone',
					'type' => 'text_datetime_timestamp_timezone',
				),
				array(
					'name'     => __( 'Attendees', 'cmb2-example-plugin' ),
					'desc'     => __( 'field description (optional)', 'cmb2-example-plugin' ),
					'id'       => $prefix . 'pets',
					'type'     => 'group',
					'options'     => array(
						'group_title'   => __( 'Attendee #{#}', 'cmb2-example-plugin' ),
						'add_button'    => __( 'Add Another Attendee', 'cmb2-example-plugin' ),
						'remove_button' => __( 'Remove Attendee', 'cmb2-example-plugin' ),
						'sortable'      => true, // beta
					),
					'fields'   => array(
						array(
							'name'     => __( 'Name', 'cmb2-example-plugin' ),
							//'desc'     => __( 'field description (optional)', 'cmb2-example-plugin' ),
							'id'       => 'name',
							'type'     => 'text_medium',
						),
						array(
							'name'     => __( 'Picture', 'cmb2-example-plugin' ),
							//'desc'     => __( 'field description (optional)', 'cmb2-example-plugin' ),
							'id'       => 'email',
							'type'     => 'text_email',
						),


					)
				)

			)
		);*/

		//return (array) $meta_boxes;
	}
}
add_filter( 'cmb2_meta_boxes', 'event_meta' );