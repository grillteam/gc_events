<?php
namespace Grillcode;

if( !class_exists( 'GCFields' ) ){

    class GCFields{

        public $fields;

        const VERSION = '1.0.0';

        private static $_instance;

        public static function getInstance() {
            if ( !self::$_instance ) {
                self::$_instance = new GCFields();
            }

            return self::$_instance;
        }

        public function version() {
            return self::VERSION;
        }

        public function __construct(){

        }

        /**
         * Renders a field control
         *
         * Generates the control for a field
         *
         * @param string $fieldName is the id/name of the control
         * @param string $fieldValue is the value of the control
         * @param string $fieldLabel is the label of the control
         * @param string $fieldType is the type of the control(text, checkbox, number)
         * @param string $fieldDesc is the description of the field
         * @param int $fieldSize is the control size
         * @param int $maxlength is the control maxlength
         * @param array $options is the control options
         * @return nothing
         */
        public static function pxRenderField( $fieldName, $fieldValue=null, $fieldLabel, $fieldType='text', $fieldDesc='', $fieldSize=60, $maxlength=null, $options=null ){


            switch ($fieldType) {
                case 'text':
                    echo '<tr><td><label for="' . $fieldName . '">' . $fieldLabel . '</label></td>';
                    echo '<td>';
                    echo '<input type="text" name="' . esc_attr( $fieldName ) . '" id="' . esc_attr( $fieldName ) . '" value="' . esc_attr( $fieldValue ) . '" class="regular-text"  style="width: ' . $fieldSize . ';"  maxlength="' . $maxlength . '" />';
                    echo '<div class="field-desc">';
                    echo $fieldDesc ;
                    echo '</div>';
                    echo '</td></tr>';
                    break;
                case 'checkbox':
                    echo '<tr><td></td>';
                    echo '<td>';
                    echo '<input type="checkbox" name="' . esc_attr( $fieldName ) . '" id="' . esc_attr( $fieldName ) . '" ' . checked( $fieldValue, true, false ) . ' value="1" />';
                    echo '<label for="' . esc_attr( $fieldName ) . '">' . $fieldDesc . '</label>';
                    echo '</td></tr>';
                    break;
                case 'number':
                    echo '<tr><td><label for="' . $fieldName . '">' . $fieldLabel . '</label></td>';
                    echo '<td>';
                    echo '<input type="text" name="' . esc_attr( $fieldName ) . '" id="' . esc_attr( $fieldName ) . '" value="' . esc_attr( $fieldValue ) . '" class="regular-text" style="width: ' . $fieldSize . ';"  maxlength="' . $maxlength . '" />';
                    echo '<div class="field-desc">';
                    echo $fieldDesc ;
                    echo '</div>';
                    echo '</td></tr>';
                    break;
                case 'textarea':
                    echo '<tr><td><label for="' . $fieldName . '">' . $fieldLabel . '</label></td>';
                    echo '<td>';
                    echo '<textarea name="' . esc_attr( $fieldName ) . '" id="' . esc_attr( $fieldName ) . '" cols="60" rows="4">' . esc_textarea( $fieldValue ) . '</textarea>';
                    echo '<div class="field-desc">';
                    echo $fieldDesc ;
                    echo '</div>';
                    echo '</td></tr>';
                    break;
                case 'editor':
                    echo '<tr><td><label for="' . $fieldName . '">' . $fieldLabel . '</label></td>';
                    echo '<td>';
                    echo wp_editor( $fieldValue, $fieldName, array('textarea_rows' => 5, 'media_buttons' => false ));
                    echo '<div class="field-desc">';
                    echo $fieldDesc ;
                    echo '</div>';
                    echo '</td></tr>';
                    break;
                case 'select':
                    echo '<tr><td><label for="' . $fieldName . '">' . $fieldLabel . '</label></td>';
                    echo '<td>';
                    echo '<select name="' . esc_attr( $fieldName ) . '" id="' . esc_attr( $fieldName ) . '"  style="width: ' . $fieldSize . ';" >
							<option value="">Select One</option>'; // Select One
                    if(isset($options)){
                        foreach ( $options as $option )
                            echo '<option' . selected( $fieldValue, $option['value'], false ) . ' value="' . $option['value'] . '">' . $option['label'] . '</option>';
                    }
                    echo '</select>';
                    echo '<div class="field-desc">';
                    echo $fieldDesc ;
                    echo '</div>';
                    echo '</td></tr>';
                    break;
                case 'date':
                    echo '<tr><td><label for="' . $fieldName . '">' . $fieldLabel . '</label></td>';
                    echo '<td>';
                    echo '<input type="text" name="' . esc_attr( $fieldName ) . '" id="' . esc_attr( $fieldName ) . '" data-field="' . esc_attr( $fieldName ) . '" value="' . esc_attr( $fieldValue ) . '" class="regular-text date-field"  style="width: ' . $fieldSize . ';"  maxlength="' . $maxlength . '" />';
                    echo '<div class="field-desc">';
                    echo $fieldDesc ;
                    echo '</div>';
                    echo '</td></tr>';
                    break;
                case 'file':
                    echo '<tr><td><label for="' . $fieldName . '">' . $fieldLabel . '</label></td>';
                    echo '<td>';
                    echo '	<input type="text" name="' . esc_attr( $fieldName ) . '" id="' . esc_attr( $fieldName ) . '" value="' . esc_attr( $fieldValue ) . '" class="regular-text"  style="width: ' . $fieldSize . ';"  maxlength="' . $maxlength . '" />';
                    echo '	<input id="upload_' . $fieldName . '" class="button btnupload" type="button" value="Upload" />';
                    echo '<div class="field-desc">';
                    echo $fieldDesc ;
                    echo '</div>';
                    echo '</td></tr>';

                    break;
                case 'radio':
                    echo '<tr><td><label for="' . $fieldName . '">' . $fieldLabel . '</label></td>';
                    echo '<td>';
                    echo '	<ul class="meta_box_items">';
                    foreach ( $options as $option ){
                        echo '	<li><input type="radio" name="' . $fieldName . '" id="' . $fieldName . '-' . $option['value'] . '" value="' . $option['value'] . '" ' . checked( esc_attr( $fieldValue ), $option['value'], false ) . ' />
								<label for="' . $fieldName . '-' . $option['value'] . '">' . $option['label'] . '</label></li>';
                    }
                    echo '	</ul>';
                    echo '<div class="field-desc">';
                    echo $fieldDesc ;
                    echo '</div>';
                    echo '</td></tr>';
                    break;

            }

        }

        /**
         * Creates a new metabox
         *
         * @param array $fields
         * @return nothing
         */
        public static function generate_metabox_fields( $fields ){
            global $post;

            echo '<table class="gc-fields-table">';
            foreach ( $fields as $field ) :


                $id 		= isset( $field['id'] ) ? $field['id'] : null;
                $label 		= isset( $field['label'] ) ? $field['label'] : null;
                $desc 		= isset( $field['desc'] ) ? '<span class="description">' . $field['desc'] . '</span>' : null;
                $type 		= isset( $field['type'] ) ? $field['type'] : null;
                $size 		= isset( $field['size'] ) ? $field['size'] : 10;
                $maxlength	= isset( $field['maxlength'] ) ? $field['maxlength'] : null;
                $options 	= isset( $field['options'] ) ? $field['options'] : null;
                $meta 		= get_post_meta( $post->ID, $id, true);

                switch( $type ) {
                    case 'text':
                        self::pxRenderField( $id, $meta, $label, 'text', $desc, $size, $maxlength );
                        break;
                    case 'checkbox':
                        self::pxRenderField( $id, $meta, $label, 'checkbox', $desc );
                        break;
                    case 'number':
                        self::pxRenderField( $id, $meta, $label, 'number', $desc, $size, $maxlength );
                        break;
                    case 'textarea':
                        self::pxRenderField( $id, $meta, $label, 'textarea', $desc );
                        break;
                    case 'editor':
                        self::pxRenderField( $id, $meta, $label, 'editor', $desc );
                        break;
                    case 'select':
                        self::pxRenderField( $id, $meta, $label, 'select', $desc, null, $maxlength, $options );
                        break;
                    case 'radio':
                        self::pxRenderField( $id, $meta, $label, 'radio', $desc, null, $maxlength, $options );
                        break;
                    case 'date':
                        self::pxRenderField( $id, $meta, $label, 'date', $desc, $size );
                        break;
                    case 'image':
                        self::pxRenderField( $id, $meta, $label, 'file', $desc );
                        break;
                    case 'file':
                        self::pxRenderField( $id, $meta, $label, 'file', $desc, $size );
                        break;

                }

            endforeach;

            echo '</table>';
        } // generate_metabox_fields



        /**
         * Saves metabox data
         *
         * @param array $fields
         * @return nothing
         */
        public static function save_metabox_fields( $fields ){
            global $post;

            $post_id = (isset($post->ID))?$post->ID:0;
            $prefix = '';

            // check autosave
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
                return $post_id;
            // check permissions
            if ( ! current_user_can( 'edit_page', $post_id ) )
                return $post_id;

            foreach ( $fields as $field) {

                if ( $field['type']=='checkbox' ){

                    if ( isset($_POST[$prefix . $field['id']] ) )
                        $data =  $_POST[$prefix . $field['id']] ;
                    else
                        $data = 0;

                    update_post_meta( $post_id, $prefix . $field['id'], $data );

                } elseif  ( $field['type']=='repeatable' ) {

                    $old = get_post_meta($post_id, $prefix . $field['id'], true);
                    $new = array();

                    $rep = $_POST[ $field['id']];
                    var_dump($rep);

                    $count = count( $rep );

                    for ( $i = 0; $i < $count; $i++ ) {
                        if ( $rep[$i] != '' ) :
                            $new[$i][$field['id']] = stripslashes( strip_tags( $rep[$i] ) );

                        endif;
                    }

                    if ( !empty( $new ) && $new != $old )
                        update_post_meta( $post_id, $prefix . $field['id'], $new );
                    elseif ( empty( $new ) && $old )
                        delete_post_meta( $post_id, $prefix . $field['id'], $old );

                } elseif ( isset( $_POST[$prefix . $field['id']] ) ){
                    $data =  $_POST[$prefix . $field['id']] ;
                    update_post_meta( $post_id, $prefix . $field['id'], $data );
                }
            }

        } // save_metabox_fields

    } //class
}