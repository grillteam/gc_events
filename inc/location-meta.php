<?php
/**
 * Add additional meta to Location cpt
 */
if( ! function_exists( 'location_meta' ) ) {
    function location_meta( ) {
        $prefix = '_gc_location_meta_';

        $cmb = new_cmb2_box( array(
            'id'           => 'location-meta',
            'title'        => __( 'Location data', 'grillcode' ),
            'object_types' => array( 'gc-location' ),
            'show_names'   => true
        ) );

        $cmb->add_field( array(
            'name' => __( 'Address 1','grillcode' ),
            'id'   => $prefix . 'address1',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => __( 'Address 2','grillcode' ),
            'id'   => $prefix . 'address2',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => __( 'City','grillcode' ),
            'id'   => $prefix . 'city',
            'type' => 'text_medium'
        ) );

        $cmb->add_field( array(
            'name' => __( 'State','grillcode' ),
            'id'   => $prefix . 'state',
            'type' => 'text_medium'
        ) );

        $cmb->add_field( array(
            'name' => __( 'Zip','grillcode' ),
            'id'   => $prefix . 'zip',
            'type' => 'text_small'
        ) );

        $cmb->add_field( array(
            'name' => __( 'Phone','grillcode' ),
            'id'   => $prefix . 'phone',
            'type' => 'text_small'
        ) );

        $cmb->add_field( array(
            'name' => __( 'Webpage','grillcode' ),
            'id'   => $prefix . 'webpage',
            'type' => 'text_url'
        ) );

        $cmb->add_field( array(
            'name' => __( 'URL Text','grillcode' ),
            'id'   => $prefix . 'url_test',
            'type' => 'text'
        ) );


    }
}
add_filter( 'cmb2_meta_boxes', 'location_meta' );